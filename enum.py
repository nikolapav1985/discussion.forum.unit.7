#!/usr/bin/python

"""
FILE enum.py

enumerate tuple elements examples

OUTPUT EXAMPLE

(0, 0)
(1, 1)
(2, 4)
(3, 9)
(4, 16)
(5, 25)
(6, 36)
(7, 49)
(8, 64)
(9, 81)

index, item pairs (index position of item in a list)
"""

if __name__ == "__main__":
    lst = [0,1,4,9,16,25,36,49,64,81]
    
    for i,item in enumerate(lst):
        print(i,item)
