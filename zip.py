#!/usr/bin/python

"""
FILE zip.py

zip lists example

OUTPUT EXAMPLE 

(0, 0)
(1, 1)
(2, 4)
(3, 9)
(4, 16)
(5, 25)
(6, 36)
(7, 49)
(8, 64)
(9, 81)

each line is a tuple (first element from first list, second element from second list)
"""

if __name__ == "__main__":
    lsta = [0,1,2,3,4,5,6,7,8,9]
    lstb = [0,1,4,9,16,25,36,49,64,81]
    
    for item in zip(lsta,lstb):
        print(item)
