#!/usr/bin/python

"""
FILE dct.py

dictionary examples

OUTPUT EXAMPLE

---> populate dictionary
[(1, 1), (2, 4), (3, 9), (4, 16), (5, 25), (6, 36), (7, 49)]
---> inverted dictionary
[(1, 1), (36, 6), (9, 3), (16, 4), (49, 7), (25, 5), (4, 2)]
"""

import sys

def popsquare(dct,mx):
    """
        method popsquare

        populate dictionary using squares of numbers

        treat dictionary as a function f(x)=x^2

        can be useful as a lookup (find value in a dictionary rather than compute)

        parameters
        
        dct(dictionary) a dictionary to use
        mx(integer) a maximum number to use
    """
    for i in range(1,mx):
        dct[i]=i*i

def invertdict(dct,inv):
    """
        method invertdict

        invert a dictionary (use values as keys)

        parameters

        dct(dictionary) a dictionary to use
        inv(dictionary) an inverted dictionary
    """
    item = None

    for key in dct:
        item = dct[key]
        if item not in inv:
            inv[item]=key
        else:
            inv[item].append(key)

if __name__ == "__main__":
    mx = None
    dct = dict()
    inv = dict()

    if len(sys.argv) != 2:
        print("Usage ./scriptname N")
        exit(1)

    mx = int(sys.argv[1])
    popsquare(dct,mx)
    print("---> populate dictionary")
    print(dct.items())
    invertdict(dct,inv)
    print("---> inverted dictionary")
    """
        in both cases output is list of tuples. tuples are key, value pairs found in original dictionaries.
    """
    print(inv.items())
